import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlertModule} from "ngx-bootstrap/alert";
import { HomePageComponent } from './components/home-page/home-page.component';
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule } from '@angular/common/http';
import { DetailsDayWeatherComponent } from './components/details-day-weather/details-day-weather.component';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    DetailsDayWeatherComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
