import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {WeatherDayItemModel} from '../../models/weatherModel';
import {DecimalPipe} from '@angular/common';

@Component({
  selector: 'app-details-day-weather',
  templateUrl: './details-day-weather.component.html',
  styleUrls: ['./details-day-weather.component.scss'],
  providers: [DecimalPipe]
})
export class DetailsDayWeatherComponent implements OnInit, OnChanges {
  @Input() dayWeather: WeatherDayItemModel;

  constructor() {
  }

  ngOnInit(): void {

  }

  ngOnChanges() {
    console.log('y[2] dayWeather', this.dayWeather);
  }

}
