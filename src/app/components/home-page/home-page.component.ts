import {Component, OnDestroy, OnInit} from '@angular/core';
import * as moment from 'moment';
import {WeatherService} from '../../services/weather.service';
import {WeatherDayItemModel} from '../../models/weatherModel';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit, OnDestroy {
  public availableDays: any[] = [];
  public city: string = 'Zhytomyr';
  // city: string;
  public weatherData: Array<WeatherDayItemModel>;
  public dayWeather: WeatherDayItemModel | null;

  constructor(
    private weatherService: WeatherService
  ) {
  }

  ngOnInit(): void {
    this.checkWeatherInLocalStorage();
    this.getAvailableDays();
  }

  checkWeatherInLocalStorage(): void {
    let permanentWeather = JSON.parse(<string> localStorage.getItem('weatherData'));
    if (permanentWeather) {
      this.weatherData = permanentWeather;
      // console.log('y[1] weather from Local', this.weatherData);
    }
  }

  ngOnDestroy(): void {
    //   localStorage.removeItem('weatherData'); // no NgOnDestroy in this App
  }

  getAvailableDays(): void {
    for (let i = 0; i < 3; i++) {
      moment.locale('uk');
      let day = {
        dayOfTheWeek: moment().add(i, 'days').format('dddd'),
        dayNumber: moment().add(i, 'days').format('D'),
        month: moment().add(i, 'days').format('MMMM')
      };
      this.availableDays.push(day);
    }
    this.availableDays[0].selected = true;
    this.setDayWeather(this.weatherData ? this.weatherData[0] : null);
  }

  setDayWeather(dayWeather: WeatherDayItemModel | null): void {
    this.dayWeather = dayWeather || null;
  }

  selectAnotherDay(dayIndex): void {
    this.availableDays.map((day) => day.selected = false);
    this.availableDays[dayIndex].selected = true;
    this.setDayWeather(this.weatherData ? this.weatherData[dayIndex] : null);
  }

  searchWeatherForCity(): void {
    forkJoin(
      this.weatherService.getFirstApiWeatherData(this.city),
      this.weatherService.getSecondApiWeatherData(this.city)
    ).subscribe((res) => {
      // console.log('y[11] res', res);
      // this.weatherData = this.weatherService.getGeneralWeatherData(res[0], res[1]);
      // localStorage.setItem('generalWeatherData', JSON.stringify(this.weatherData));
      // console.log('y[11] weatherData', this.weatherData);
      // this.setDayWeather(this.weatherData[0]);
    });

    let weatherData = JSON.parse(<string> localStorage.getItem('generalWeatherData'));
    this.weatherData = weatherData;
    console.log('y[11] weatherData', weatherData);
    this.setDayWeather(this.weatherData[0]);
  }

}
